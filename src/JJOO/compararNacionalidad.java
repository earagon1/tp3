package JJOO;

import java.util.Comparator;

public class compararNacionalidad implements Comparator<Atleta> {

	@Override
	public int compare(Atleta arg0, Atleta arg1) {
		return arg0.getNacionality().compareTo(arg1.getNacionality());
	}

}
