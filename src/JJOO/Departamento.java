package JJOO;

public class Departamento {
	private Atleta[] dep;

	Departamento(){
		dep=new Atleta[4];
	}
	
	public void agregarAtleta(Atleta a) {
		if(esValido()==true) {
			int aux;
			int i=0;
			while (i <dep.length) {
				if(dep[i]==null) {
					aux=i;
					break;
				}
				i++;
			}
			dep[i]=a;
		}
	}

	public boolean esValido() {
	    if(dep.length<4) {
	    	return true;
	    }
	    return false;
	}
}
