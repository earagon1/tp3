package JJOO;

import java.util.Comparator;

public class compararGenero implements Comparator<Atleta> {

	@Override
	public int compare(Atleta arg0, Atleta arg1) {

		return arg0.getGenre().compareTo(arg1.getGenre());
	}

}
