package JJOO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class solver {

	private ArrayList<Atleta> listaAtletas = new ArrayList<Atleta>();
	private ArrayList<Departamento> listaDepartamentos = new ArrayList<Departamento>();
	private LinkedList<Atleta> femenino = new LinkedList<Atleta>();
	private LinkedList<Atleta> masculino = new LinkedList<Atleta>();
	private ArrayList<Atleta> temp = new ArrayList<Atleta>();

	//ordenamos la  lista por genero, primero femenino y luego masculinos
	public void ordenarPorGenero() {
		Collections.sort(listaAtletas, new compararGenero());
	}

	//duplicamos el array original 
	public ArrayList<Atleta> duplicarListaAtletas() {
		Collections.copy(temp, listaAtletas);
		return temp;
	}

	//llena el array femenino con atletas femaninas y array masculino con altetas masculinos
	public void repartirPorGenero(){
		for(Atleta a:temp){
			if(a.getGenre()=="femenino"){
				femenino.add(a);
			}else{
				masculino.add(a);
			}
		}
	}
	
	//ordena por nacionalidad tanto el array femenino como masculino
	public void ordenarPorNacionalidad(){
		Collections.sort(femenino,new compararNacionalidad());
		Collections.sort(masculino,new compararNacionalidad());
	}
//falta transformar el json en arraylist
	public void resolvedor(){
		duplicarListaAtletas();
		ordenarPorGenero();
		repartirPorGenero();
		ordenarPorNacionalidad();

	}
	
	
	public static void main(String[] args) {
		
	}

}
